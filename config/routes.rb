Rails.application.routes.draw do
  
  root to: "sessions#new"
  
  resources :users do
      resources :circles, only: [:index, :new, :create]
  end
  resources :circles, except: [:index, :new, :create]
  resources :posts
  resource :session, only: [:new, :create, :destroy]
  
  get "feed", to: "users#feed"
  
end
