class UsersController < ApplicationController
  before_action :require_login, only: [:show]
  before_action :require_logout, only: [:new, :create]
  
  def new
    @user = User.new
    render "new"
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      log_user_in!(@user)
      redirect_to user_url(@user)
    else
      flash.now[:errors] = @user.errors.full_messages
      render "new"
    end
  end
  
  def show
    @user = User.find(params[:id])
    render "show"
  end
  
  def feed
    @posts = []
    current_user.circles.each do |circle|
      @posts += circle.posts
    end
    render "feed"
  end
  
  private
  
  def user_params
    params.require(:user).permit(:email, :password)
  end
end
