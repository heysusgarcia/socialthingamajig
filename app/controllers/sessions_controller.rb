class SessionsController < ApplicationController
  
  before_action :require_logout, only: [:new, :create]
  
  def new
    render "new"
  end
  
  def create
    @user = User.find_by_credentials(session_params)
    if @user.nil?
      flash.now[:errors] = ["Invalid Email or Password"]
      render "new"
    else
      log_user_in!(@user)
      redirect_to user_url(@user)
    end
  end
  
  def destroy
    current_user.reset_session_token!
    session[:session_token] = nil
    redirect_to new_session_url
  end
  
  private
  def session_params
    params.require(:session).permit(:email, :password)
  end

end
