class CirclesController < ApplicationController
  before_action :verify_circle_owner, only: [:edit, :update]
  before_action :require_login 
  
  # def index
  #   @circles = Circle.where(user_id: current_user.id)
  #   render "index"
  # end
  
  def show
    @circle = Circle.find(params[:id])
    render "show"
  end
 
  def new
    @circle = Circle.new
    @user = current_user
    render "new"
  end
  
  def create
    @user = current_user
    @circle = @user.owned_circles.build(circle_params)
    if @circle.save
      redirect_to circle_url(@circle)
    else
      flash.now[:errors] = @circle.errors.full_messages
      render "new" 
    end
  end
  
  def edit
    @circle = Circle.find(params[:id])
    @user = current_user
    render "edit"
  end
  
  def update
    if @circle.update_attributes(circle_params)
      redirect_to circle_url(@circle)
    else
      flash.now[:errors] = @circle.errors.full_messages
      render "edit"
    end
  end
  
  def destroy
    @circle = Circle.find(params[:id])
    @circle.destroy
    redirect_to user_url(current_user)
  end
  
  private
  def circle_params
    params.permit(circle: [:name, user_ids: []]).require(:circle)
  end
  
  def verify_circle_owner
    @circle = Circle.find(params[:id])
    redirect_to circle_url(@circle) unless current_user == @circle.owner
  end
  

end
