class PostsController < ApplicationController
  def new
    @user = current_user
    render "new"
  end
  
  def create
    @user = current_user
    @post = @user.posts.build(post_params)
    @links = @post.links.build(link_params)
    @post_shares = @post.post_shares.build(post_shares_params)
    if @post.save
      redirect_to post_url(@post)
    else
      flash.now[:errors] = @post.errors.full_messages
      render 'new'
    end
  end
  
  def show
    @post = Post.find(params[:id])
    render "show"
  end
  
  private
  def post_params
    params.require(:post).permit(:body)
  end
  
  def link_params
    params.permit(links: [:title, :url]).require(:links).values
    .reject { |data| data.values.all?(&:blank?) }
  end
  
  def post_shares_params
    params.permit(post_shares: [ :circle_id ]).require(:post_shares)
  end
end
