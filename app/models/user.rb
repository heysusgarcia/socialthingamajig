# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  email           :string(255)      not null
#  password_digest :string(255)      not null
#  session_token   :string(255)      not null
#  created_at      :datetime
#  updated_at      :datetime
#

class User < ActiveRecord::Base
  validates :email, :session_token, :password_digest, 
            presence: true, uniqueness: true
  validates :password, length: { minimum: 6, allow_nil: true }
  before_validation :ensure_session_token
  
  has_many :owned_circles, foreign_key: :user_id, class_name: :Circle, inverse_of: :owner
  has_many :circle_memberships, inverse_of: :user
  has_many :circles, through: :circle_memberships, source: :circle
  
  has_many :posts, inverse_of: :user
  
  def password=(secret)
    @password = secret
    self.password_digest = BCrypt::Password.create(secret)
  end
  
  def is_password?(secret)
    BCrypt::Password.new(self.password_digest).is_password?(secret)
  end
  
  def self.generate_session_token
    SecureRandom::urlsafe_base64(16)
  end
  
  def reset_session_token!
    self.session_token = self.class.generate_session_token
    self.save!
    self.session_token
  end
  
  def self.find_by_credentials(credentials)
    @user = User.find_by_email(credentials[:email])
    return nil if @user.nil?
    return @user if @user.is_password?(credentials[:password])
    nil
  end
  
  private
  attr_reader :password
  
  def ensure_session_token
    self.session_token ||= self.class.generate_session_token
  end
  
end
