# == Schema Information
#
# Table name: circles
#
#  id         :integer          not null, primary key
#  name       :string(255)      not null
#  user_id    :integer          not null
#  created_at :datetime
#  updated_at :datetime
#

class Circle < ActiveRecord::Base
  validates :owner, :name, presence: true
  has_many :circle_memberships, inverse_of: :circle
  has_many :users, through: :circle_memberships, source: :user
  belongs_to :owner, foreign_key: :user_id, class_name: :User,
                              inverse_of: :owned_circles
  has_many :post_shares, inverse_of: :circle
  has_many :posts, through: :post_shares, source: :post
end
