# == Schema Information
#
# Table name: circle_memberships
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  circle_id  :integer          not null
#  created_at :datetime
#  updated_at :datetime
#

class CircleMembership < ActiveRecord::Base
  validates :user, :circle, presence: true
  belongs_to :user, inverse_of: :circle_memberships
  belongs_to :circle, inverse_of: :circle_memberships
end
